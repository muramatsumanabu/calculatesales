package jp.muramatsu_manabu.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		//コマンドライン引数が1以外ならエラー
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchName = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		Map<String, String> commodityName = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		if (!inputFile(args[0], "branch.lst", "^[0-9]{3}$", "支店", branchName, branchSales)) {
			return;
		} //
		if (!inputFile(args[0], "commodity.lst", "^[A-Za-z0-9]+$", "商品", commodityName, commoditySales)) {
			return;
		}

		//regexに該当しかつファイルであれば、をリストに格納
		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().matches("[0-9]{8}.rcd$") && files[i].isFile()) {
				rcdFiles.add(files[i]);
			}
		}
		//売上ファイルリストのソート
		Collections.sort(rcdFiles);

		for (int i = 0; i < rcdFiles.size() - 1; i++) {

			//現在のファイルと次のファイル名の名前を取得
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		BufferedReader br = null;

		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));

				//作成したListに、支店コードと売上を格納する
				List<String> fileList = new ArrayList<>();
				String line;
				while ((line = br.readLine()) != null) {
					fileList.add(line);
				}
				//売上ファイルが3行以外であれば、エラー
				if (fileList.size() != 3) {
					System.out.println(rcdFiles.get(i) + "のフォーマットが不正です");
					return;
				}
				//代入された支店コードが支店定義ファイルになかった場合エラー
				if (!(branchName.containsKey(fileList.get(0)))) {
					System.out.println(rcdFiles.get(i) + "の支店コードが不正です");
					return;
				}
				//代入された商品コードが商品定義ファイルになかった場合エラー
				if (!(commodityName.containsKey(fileList.get(1)))) {
					System.out.println(rcdFiles.get(i) + "の商品コードが不正です");
					return;
				}

				//支店コード
				String key = fileList.get(0);

				//商品コード
				String item = fileList.get(1);

				//リストで渡された売上が、数字でない場合エラー
				if (!fileList.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました1");
					return;
				}

				//リストの要素２を代入し、加算する
				Long fileSale = Long.valueOf(fileList.get(2));
				Long saleAmonth = branchSales.get(key) + fileSale;
				//商品コード毎に、売り上げを加算する。
				Long itemSaleAmonth = commoditySales.get(item) + fileSale;

				//売上が10桁以上ならエラー
				Long errorNum = 1000000000L;
				if (errorNum <= saleAmonth) {
					System.out.println("合計金額が10桁を超えました");
					return;
				} else if (errorNum <= itemSaleAmonth) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.put(key, saleAmonth);
				commoditySales.put(item, itemSaleAmonth);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました2");
				return;
			} finally {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました3");
					return;
				}
			}
			return;
		}

		//メソッド分け２
		if (!outputFile(args[0], "branch.out", branchName, branchSales)) {
			return;
		}
		if (!outputFile(args[0], "commodity.out", commodityName, commoditySales)) {
			return;
		}

	}

	//定義ファイルの読み込み
	public static boolean inputFile(String commandLine, String fileName, String regex,
			String definition, Map<String, String> nameMap, Map<String, Long> saleMap) {
		BufferedReader br = null;

		try {
			File file = new File(commandLine, fileName);

			if (!file.exists()) {
				System.out.println(definition + "定義ファイルが存在しません");
				return false;

			}

			br = new BufferedReader(new FileReader(file));

			String line;
			while ((line = br.readLine()) != null) {
				String[] datas = line.split(",");

				//split()で2分割できなかった、または正規表現とマッチしなければエラー
				if ((datas.length != 2) || (!datas[0].matches(regex))) {
					System.out.println(definition + "定義ファイルのフォーマットが不正です");
					return false;
				}

				//マップのキーを同時に格納
				nameMap.put(datas[0], datas[1]);
				saleMap.put(datas[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} catch (NullPointerException e) {
				return false;
			}
		}
		return true;
	}

	//集計処理メソッド
	public static boolean outputFile(String commandLine, String fileName, Map<String, String> nameMap, Map<String, Long> saleMap) {

		File file = new File(commandLine, fileName);

		BufferedWriter bw = null;
		try {

			bw = new BufferedWriter(new FileWriter(file));
			for (String key : saleMap.keySet()) {

				bw.write(key + "," + nameMap.get(key) + "," + saleMap.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				bw.close();
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;

	}

}